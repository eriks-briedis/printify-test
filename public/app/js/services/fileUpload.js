app.service('fileUpload', ['$http', function ($http) {
    this.uploadFileToUrl = function(file, data, callback){
       var fd = new FormData();
       
       fd.append('file', file);
       fd.append('top', data.top);
       fd.append('left', data.left);
       fd.append('width', data.width);
       fd.append('height', data.height);
    
       $http.post(data.uploadUrl, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
       }).then(function () {
            callback();
       });
    }
}]);