app.service('canvas', function () {

	this.board = new fabric.Canvas("board");
	this.rect = new fabric.Rect({
        width: 222,
        height: 259,
        top: 79,
        left: 158,
        fill: 'transparent',
        hasBorders: false,
        hasControls: false,
        lockMovementX: true,
        lockMovementY: true,
        evented: false,
        selectable: false
    });

    this.init = function () {
    	this.loadBackground();
    	this.board.add(this.rect);
    }

    this.loadBackground = function () {
    	var self = this,
    		shirt = new Image();
    	shirt.onload = function (event) {
	        self.board.setBackgroundImage(shirt.src, self.board.renderAll.bind(self.board), {
	            originX: 'left',
	            originY: 'top',
	            left: 0,
	            top: 0
	        });
	    }
	    shirt.src = 'app/images/shirt.png';
    }

    this.upload = function ($scope, img, callback) {

    	var self      = this,
    		reader    = new FileReader,
            f         = document.getElementById('file').files[0],
            maxWidth  = 222,
            maxHeight = 259;
            
        reader.onload = function () {
            
            img.onload = function (event) {

                image = new fabric.Image(img);

                var printRatio = maxWidth / 4500,
                	_width = image.getBoundingRectWidth() * printRatio,
                	_height = image.getBoundingRectHeight() * printRatio,
                	ratio = 1;

                if (_width > maxWidth) {
                	ratio = maxWidth / _width;
                	_width = maxWidth;
                	_height = _height * ratio;
                }

                if (_height > maxHeight) {
                	ratio = maxHeight / _height;
                	_width = _width * ratio;
                	_height = maxHeight;
                }

                image.set({
                    width: _width,
                    height: _height,
                    hasRotatingPoint: false,
                    top: 0,
                    left: 0
                });

                self.board.centerObject(image);
                self.board.add(image);
                self.board.renderAll();

                callback(image, printRatio);
            }
            img.src = event.target.result;
        }
        reader.readAsDataURL(f);
    }

    this.moving = function ($scope, image) {

    	var top         = image.top,
            bottom      = top + image.height,
            left        = image.left,
            right       = left + image.width,
            topBound    = this.rect.top,
            bottomBound = topBound + this.rect.height,
            leftBound   = this.rect.left,
            rightBound  = leftBound + this.rect.width;

        $scope.posLeft = Math.min(Math.max(left, leftBound), rightBound - parseInt(parseInt(image.width) * image.scaleX));
        $scope.posTop = Math.min(Math.max(top, topBound), bottomBound - parseInt(parseInt(image.height) * image.scaleY));

    	image.setLeft($scope.posLeft);
      	image.setTop($scope.posTop);
    }

    this.scaling = function ($scope, image) {

        $scope.width = parseInt(parseInt(image.width) * image.scaleX);
        $scope.height = parseInt(parseInt(image.height) * image.scaleY);

        if(image.left < this.rect.left || (parseInt(image.left) + $scope.width) > (parseInt(this.rect.left)+parseInt(this.rect.width))) {
            image.setLeft(this.rect.left);
            image.setScaleX((this.rect.width/image.width));
        }

        if(image.top < this.rect.top || (parseInt(image.top) + $scope.height) > (parseInt(this.rect.top)+parseInt(this.rect.height))){
            image.setTop(this.rect.top);
            image.setScaleY((this.rect.height/image.height));
        }   
    }
});