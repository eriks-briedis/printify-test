app.controller('MainController', ['$scope', 'fileUpload', 'canvas' , function ($scope, fileUpload, canvas) {
    
    var img = new Image(),
        image,
        printRatio;

    canvas.init();
    
    $scope.posLeft = 0;
    $scope.posTop  = 0;
    $scope.width   = 0;
    $scope.height  = 0;

    $scope.upload = function () {

        if (typeof($scope.image) == 'undefined') {
            alert('You upload an image first');
            return false;
        }

        if (typeof (image) != 'undefined') {
            image.remove();
        }

        canvas.upload($scope, img, function (res, ratio) {
            image = res;
            printRatio = ratio;
            canvas.moving($scope, image);
            canvas.scaling($scope, image);
        });
    }

    $scope.save = function () {

        if (typeof(image) == 'undefined') {
            alert('You upload an image first');
            return false;
        }
        
        $('.loader-wrap').removeClass('hidden');

    	var data = {
    		top: $scope.posTop / printRatio,
    		left: $scope.posLeft / printRatio,
    		width: $scope.width / printRatio,
    		height: $scope.height / printRatio,
    		uploadUrl: '/upload'
    	};

        fileUpload.uploadFileToUrl($scope.image, data, function () {
            image.remove();
            $('.loader-wrap').addClass('hidden');
            $('#file').val('');
            alert('Image saved successfully');
        });
    }

    canvas.board.on("object:moving", function () {
        canvas.moving($scope, image)
    });

    canvas.board.observe("object:scaling", function () {
        canvas.scaling($scope, image);
    });
}]);