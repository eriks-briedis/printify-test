<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Product;

class AdminController extends Controller
{
    public function index()
    {
    	return view('index', array('products' => Product::all()));
    }

    public function product($id = false)
    {
    	return view('product', array('product' => Product::find($id)));
    }
}
