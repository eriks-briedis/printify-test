<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Validator;
use App\Product;
use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken;

class UploadsController extends Controller
{
    public function index()
    {
    }

    public function store()
    {
    	$file = array('image' => Input::file('file'));
    	$rules = array('image' => 'required');
    	$validator = Validator::make($file, $rules);
    	if (!$validator->fails()) {
    		if (Input::file('file')->isValid()) {
    			$destinationPath = 'uploads';
		    	$extension = Input::file('file')->getClientOriginalExtension();
		      	$fileName = rand(11111,99999).'.'.$extension;
		      	Input::file('file')->move($destinationPath, $fileName);

		      	$product = new Product;
		      	$product->width = Input::get('width');
		      	$product->height = Input::get('height');
		      	$product->top = Input::get('top');
		      	$product->left = Input::get('left');
		      	$product->image = $fileName;
		      	$product->save();

    			return response()->json(array('status' => 'success', 'message' => 'Image uploaded successfully'));
    		}
    	}
    	return response()->json(array('status' => 'error', 'message' => 'Not a valid image file'));
    }
}
