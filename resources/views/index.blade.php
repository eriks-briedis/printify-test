@extends('layouts.master')
@section('title', 'Home')

@section('content')
	<table class="table table-striped">
		<thead>
			<tr>
				<th>ID</th>
				<th>Width</th>
				<th>Height</th>
				<th>Position top</th>
				<th>Position left</th>
			</tr>
		</thead> 
	    <tbody>
		    @foreach($products as $key => $product)
		    	<tr>
		    		<td>
		    			<a href="{{ URL::action('AdminController@product', $product->id) }}">{{ $product->id }}</a>
		    		</td>
		    		<td>{{ $product->width }}</td>
		    		<td>{{ $product->height }}</td>
		    		<td>{{ $product->top }}</td>
		    		<td>{{ $product->left }}</td>
		    	</tr>
		    @endforeach
	    </tbody>
    </table>
@endsection