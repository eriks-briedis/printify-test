<!DOCTYPE html>
<html>
<head>
    <title>Printify - @yield('title')</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="/app/css/admin.css" rel="stylesheet" type="text/css">
</head>
<body>
    <header>
        <nav class="navbar navbar-default">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" 
                            class="navbar-toggle collapsed" 
                            data-toggle="collapse" 
                            data-target="#main-menu">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div class="collapse navbar-collapse" id="main-menu">
                    <ul class="nav navbar-nav">
                        <li><a href="/">App</a></li>
                        <li><a href="/admin">Products</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>
    <section class="container">
        @yield('content')
    </section>
    <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
</body>
</html>