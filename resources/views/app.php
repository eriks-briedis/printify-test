<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Print</title>
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<script src="app/lib/angular/angular.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/fabric.js/1.6.0/fabric.min.js"></script>
	<link rel="stylesheet" href="app/css/app.css">
</head>
<body>
	<div ng-app="app" ng-controller="MainController">
		<div class="container">
			<h1 class="text-center">Printify me</h1>
			<div class="row controls">
				<div class="row controls">
				<div class="col-xs-4">
					<div>
						<label for="file">1. Choose file</label>
					</div>
					<input type="file" id="file" name="file" file-model="image" />
				</div>
				<div class="col-xs-4">
					<div>
						<label for="upload">2. Upload it to canvas and position it</label>
					</div>
					<button id="upload" ng-click="upload(event)" type="button" class="btn btn-success">Upload</button>
				</div>
				<div class="col-xs-4">
					<div>
						<label for="save">3. Save your shirt design</label>
					</div>
					<button id="save" class="btn btn-primary" ng-click="save()">Save</button>
				</div>
			</div>
		</div>
	    <canvas id="board" width="530" height="530"></canvas>
	    <div class="loader-wrap hidden">
	    	<img src="app/images/loader.svg" />
	    </div>
	</div>
	<script src="app/lib/jquery/dist/jquery.min.js"></script>
	<script src="app/js/app.js"></script>
	<script src="app/js/directives/fileModel.js"></script>
	<script src="app/js/services/fileUpload.js"></script>
	<script src="app/js/services/canvas.js"></script>
	<script src="app/js/controllers/mainController.js"></script>
</body>
</html>
