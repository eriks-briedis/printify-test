@extends('layouts.master')
@section('title', 'Product')

@section('content')
	<table class="table table-striped">
		<tbody>
			<tr>
				<th>ID</th>
				<td>{{ $product->id }}</td>
			</tr>
			<tr>
				<th>Image</th>
				<td><img src="/uploads/{{ $product->image }}" /></td>
			</tr>
			<tr>
				<th>Width</th>
				<td>{{ $product->width }}</td>
			</tr>
			<tr>
				<th>Height</th>
				<td>{{ $product->height }}</td>
			</tr>
			<tr>
				<th>Position top</th>
				<td>{{ $product->top }}</td>
			</tr>
			<tr>
				<th>Position left</th>
				<td>{{ $product->left }}</td>
			</tr>
		</tbody>
	</table>
@endsection